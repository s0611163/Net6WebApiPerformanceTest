# Net6WebApiPerformanceTest

.NET 6 WebApi 测试，测试了SqlSugar、FreeSql、Dapper.LiteSql、Entity Framework Core、Fast.Framework款ORM的查询性能(模糊查询、排序、分页、参数化查询)

## 数据库

MySQL 版本5.7.28

## 开发环境

VS2022
.NET 6

## JMeter版本

apache-jmeter-5.5

## 代码分支

### main
该分支测试Dapper.LiteSql
### EFCore
### SqlSugar
### FreeSql
### ADO.NET
### Fast.Framework



