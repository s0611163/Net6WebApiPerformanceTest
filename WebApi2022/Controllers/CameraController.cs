﻿using Filters;
using Microsoft.AspNetCore.Mvc;
using NLog;
using Dapper.Lite;
using Models;

namespace WebApi2022.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CameraController
    {
        private Logger _log;
        private IDbSession _session;

        public CameraController(Logger log, IDbSession session)
        {
            _log = log;
            _session = session;
        }

        [HttpGet]
        [Route("queryCameras")]
        [MyFilter]
        [MyExpFilter]
        public string QueryCameras()
        {
            try
            {
                List<PtCameraInfo> list = _session.Queryable<PtCameraInfo>().ToList();

                return "ok,count=" + list.Count;
            }
            catch (Exception ex)
            {
                return "error:" + ex.ToString();
            }
        }

    }
}
