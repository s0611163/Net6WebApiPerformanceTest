﻿using DAL;
using Filters;
using Microsoft.AspNetCore.Mvc;
using Models;
using NLog;
using Dapper.Lite;

namespace WebApi2022.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DBTestController : Controller
    {

        private readonly ILogger<WeatherForecastController> _logger;
        private Logger _log;
        private IDbSession _session;

        public DBTestController(Logger log, IDbSession session)
        {
            _log = log;
            _session = session;
            //Console.WriteLine("session hashcode=" + _session.GetHashCode());
            //Console.WriteLine("log hashcode=" + _log.GetHashCode());
        }

        [HttpGet]
        [Route("Insert")]
        [MyFilter]
        [MyExpFilter]
        public string Insert()
        {
            int count = 100;
            List<SysUser> userList = new List<SysUser>();
            try
            {
                _session.BeginTransaction();
                for (int i = 1; i <= count; i++)
                {
                    SysUser user = new SysUser();
                    user.UserName = "testUser";
                    user.RealName = "测试插入用户" + i;
                    user.Password = "123456";
                    user.CreateUserid = "1";
                    user.CreateTime = DateTime.Now;
                    userList.Add(user);
                    _session.Insert(user);
                }
                _session.CommitTransaction();
            }
            catch (Exception ex)
            {
                _session.RollbackTransaction();
                return "error";
            }

            return "ok";
        }

        [HttpGet]
        [Route("InsertThrow")]
        [MyFilter]
        [MyExpFilter]
        public string InsertThrow()
        {
            int count = 100;
            List<SysUser> userList = new List<SysUser>();
            try
            {
                _session.BeginTransaction();
                for (int i = 1; i <= count; i++)
                {
                    SysUser user = new SysUser();
                    user.UserName = "testUser";
                    user.RealName = "测试插入用户" + i;
                    user.Password = "123456";
                    user.CreateUserid = "1";
                    user.CreateTime = DateTime.Now;
                    userList.Add(user);
                    if (i == 5) throw new Exception("出错了");
                    _session.Insert(user);
                }
                _session.CommitTransaction();
            }
            catch (Exception ex)
            {
                _session.RollbackTransaction();
                return "error";
            }

            return "ok";
        }

        [HttpGet]
        [Route("query")]
        [MyFilter]
        [MyExpFilter]
        public async Task<string> Query()
        {
            try
            {
                int id = 20;
                string remark = "测试";

                var list = await _session.Queryable<SysUser>()
                    .Where(t => t.Id > id && t.RealName.Contains(remark))
                    .OrderByDescending(t => t.CreateTime)
                    .OrderBy(t => t.Id).ToPageListAsync(10, 200);

                return "ok,count=" + list.Count;
            }
            catch (Exception ex)
            {
                return "error:" + ex.ToString();
            }
        }
    }
}
