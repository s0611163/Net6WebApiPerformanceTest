﻿using Dapper.Lite;
using DBProvider;

namespace WebApi2022
{
    public class DbFactoryOracle
    {
        #region 变量
        private static IDapperLite _db = new DapperLite(
            "Data Source=(DESCRIPTION =(ADDRESS_LIST =(ADDRESS = (PROTOCOL = TCP)(HOST = 34.8.12.7)(PORT = 1521)))(CONNECT_DATA =(SERVICE_NAME = hfxl)));Persist Security Info=True;User Id=shiny;Password=shiny;",
            new OracleProvider());
        #endregion

        #region 获取 IDbSession
        /// <summary>
        /// 获取 IDbSession
        /// </summary>
        public static IDbSession GetSession()
        {
            return _db.GetSession();
        }
        #endregion

        #region 获取 IDbSession (异步)
        /// <summary>
        /// 获取 IDbSession (异步)
        /// </summary>
        public static async Task<IDbSession> GetSessionAsync()
        {
            return await _db.GetSessionAsync();
        }
        #endregion

    }
}
