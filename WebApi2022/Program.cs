using NLog;
using System.Reflection;
using DAL;
using Dapper.Lite;
using Utils;
using Models;

var builder = WebApplication.CreateBuilder();

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

await StaticClassUtil.InitStaticClasses(Assembly.GetExecutingAssembly()); //Ԥ��
var count = DbFactory.GetSession().Queryable<SysUser>().Where(t => t.Id > 20).Count();
Console.WriteLine("Ԥ�����, count=" + count);

builder.Services.AddScoped<IDbSession, IDbSession>(serviceProvider =>
{
    return DbFactory.GetSession();
});

builder.Services.AddSingleton<Logger, Logger>(serviceProvider =>
{
    return LogManager.GetCurrentClassLogger();
});

var app = builder.Build();

// Configure the HTTP request pipeline.
//if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

//app.UseHttpsRedirection();

//app.UseAuthorization();

string _basePath = Path.GetDirectoryName(Uri.UnescapeDataString(Assembly.GetExecutingAssembly().Location));
Console.WriteLine(_basePath);
LogManager.LoadConfiguration(_basePath + "/NLog.config");

app.MapControllers();

app.Run();
